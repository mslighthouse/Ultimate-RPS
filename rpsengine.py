# Globals
OPTIONS_FILE = 'options.txt'
WCOMBOS_FILE = 'winning-combos.txt'

# Load Choice Options
def load_options(optionsFile):
    options = []
    with open(optionsFile) as file:
        for line in file:
            line = line.rstrip()
            options.append(line)
    return options

# Load Winning combos
# returns a dict with keys being winning pairs [(winner, loser)] = message
def load_winning_combos(combosFile):
    combos = {}
    with open(combosFile) as file:
        for line in file:
            line = line.rstrip()
            line = line.split(',')
            winner = line[0]
            loser  = line[1]
            msg    = line[2]
            combos[(winner,loser)] = msg
    return combos

def determine_winner(options, combos, choice1, choice2):
    c1str = options[choice1]
    c2str = options[choice2]

    if choice1 == choice2: # Tie
        return 0
    elif (c1str, c2str) in combos: # Player 1 Wins
        return 1
    elif (c2str, c1str) in combos: # Player 2 Wins
        return 2
