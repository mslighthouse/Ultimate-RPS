import time
import random
import rpsengine as RPSEngine
# Import All Options and Winning Combinations
# Prompt User for what Version They would like to play

NUM_GAMES = 4

print("Welcome to Extreme Rock Paper Scissors!\nWhat game would you like to play?")
print("\t[1] = Rock, Paper, Scissors")
print("\t[2] = Rock Paper Scissors Lizard Spock")
print("\t[3] = Rock Paper Scissors Lizard Spock Spiderman Batman Wizard Glock")
print("\t[4] = EXTREME Rock Paper Scissors (25 Options!)")
game_v = -1
while (game_v < 1) or (game_v > NUM_GAMES):
    try:
        game_v = input("Enter choice of game: ")
        game_v = int(game_v)
    except:
        continue

gameName = 'v' + str(game_v)
options = RPSEngine.load_options(gameName + '/options.txt')
winning_combos = RPSEngine.load_winning_combos(gameName + '/winning-combos.txt')

# CPU Decides its choice
cpu = random.randint(0,len(options)-1)

# ---------- MAIN MECHANIC FOR USER -----------
print("Choose your fate!")
count = 0
for i in options:
    print("\t[" + str(count) + "] - " + i)
    count = count + 1

choice = -1
while (choice < 0) or (choice >= len(options)):
    try:
        choice = input("Select an option (0 - {0}): ".format(len(options)-1))
        choice = int(choice)
        if choice >= 0 and choice < len(options):
            break
        else:
            print("That is not a valid option! Try again.")
    except:
        print("That is not a valid option! Try again.")
        continue

print("You selected {0}!".format(options[choice]))

print("Rolling in 3...")
time.sleep(0.5)
print("\t   2...")
time.sleep(0.5)
print("\t   1...\n")
time.sleep(0.5)

# ---------- DETERMINE GAME RESULT ----------
player_c = options[choice]
cpu_c    = options[cpu]
result = RPSEngine.determine_winner(options, winning_combos, choice, cpu)
if result == 0:
    print("IT'S A TIE! You both chose {0}!".format(player_c))
elif result == 1:
    print(winning_combos[player_c, cpu_c])
    print("\t---- YOU WIN! ----")
elif result == 2:
    print(winning_combos[cpu_c, player_c])
    print("\t---- YOU LOSE! ---")
